-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 25, 2017 at 09:53 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apotik`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `USERNAME` char(50) NOT NULL,
  `PASSWORD` char(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`USERNAME`, `PASSWORD`) VALUES
('admin', '21232f297a57a5a743894a0e4a801fc3'),
('IPSUM', 'IPSUM'),
('LOREM', 'LOREM');

-- --------------------------------------------------------

--
-- Table structure for table `detil_transaksi`
--

CREATE TABLE `detil_transaksi` (
  `JUMLAH` int(11) DEFAULT NULL,
  `SUB_TOTAL` int(11) DEFAULT NULL,
  `KD_DETAIL` char(100) NOT NULL,
  `KD_TRANSAKSI` char(100) DEFAULT NULL,
  `KD_OBAT` char(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `obat`
--

CREATE TABLE `obat` (
  `KD_OBAT` char(100) NOT NULL,
  `USERNAME` char(50) DEFAULT NULL,
  `ID_SUPPLIER` char(100) DEFAULT NULL,
  `NAMA_OBAT` varchar(100) DEFAULT NULL,
  `JML_STOK` int(11) DEFAULT NULL,
  `HARGA` int(11) DEFAULT NULL,
  `PRODUSEN` varchar(100) DEFAULT NULL,
  `FOTO` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `obat`
--

INSERT INTO `obat` (`KD_OBAT`, `USERNAME`, `ID_SUPPLIER`, `NAMA_OBAT`, `JML_STOK`, `HARGA`, `PRODUSEN`, `FOTO`) VALUES
('OB3', NULL, 'SP1', 'Vitacimin', 19, 2300, 'pt maju mapan', '11e461e5bd433f1a2655fae1ea983079.jpg'),
('Obat1', NULL, 'SP1', 'PARAMEX', 19, 2300, 'pt kim', '6.PNG'),
('Obat2', NULL, 'SP2', 'BODREX', 15, 4500, 'PT Indovarma', 'brosur2.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `ID_SUPPLIER` char(100) NOT NULL,
  `USERNAME` char(50) DEFAULT NULL,
  `NAMA_SP` varchar(100) DEFAULT NULL,
  `ALAMAT` text,
  `KOTA` text,
  `TELP` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`ID_SUPPLIER`, `USERNAME`, `NAMA_SP`, `ALAMAT`, `KOTA`, `TELP`) VALUES
('SP1', NULL, 'LOREM', 'LOREM', 'LOREM', 0),
('SP2', NULL, 'SURYA', 'Jalan Kerinci', 'MALANG', 8977800);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `KD_TRANSAKSI` char(100) NOT NULL,
  `USERNAME` char(50) DEFAULT NULL,
  `KD_OBAT` char(100) DEFAULT NULL,
  `NAMA_OBAT` varchar(100) DEFAULT NULL,
  `TANGGAL` date DEFAULT NULL,
  `TOTAL` int(11) DEFAULT NULL,
  `JUMLAH_BELI` int(11) DEFAULT NULL,
  `ADMIN` varchar(1024) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`KD_TRANSAKSI`, `USERNAME`, `KD_OBAT`, `NAMA_OBAT`, `TANGGAL`, `TOTAL`, `JUMLAH_BELI`, `ADMIN`) VALUES
('2345', NULL, 'OB3', NULL, '2017-07-19', NULL, 6, NULL),
('345', NULL, 'Obat1', NULL, '2017-07-17', NULL, 3, NULL),
('75', NULL, 'Obat1', NULL, '2017-05-22', NULL, 46, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`USERNAME`);

--
-- Indexes for table `detil_transaksi`
--
ALTER TABLE `detil_transaksi`
  ADD PRIMARY KEY (`KD_DETAIL`),
  ADD KEY `FK_MEMILIKI` (`KD_TRANSAKSI`),
  ADD KEY `FK_RELATIONSHIP_5` (`KD_OBAT`);

--
-- Indexes for table `obat`
--
ALTER TABLE `obat`
  ADD PRIMARY KEY (`KD_OBAT`),
  ADD KEY `FK_MENGELOLA` (`USERNAME`),
  ADD KEY `FK_RELATIONSHIP_6` (`ID_SUPPLIER`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`ID_SUPPLIER`),
  ADD KEY `FK_MENGELOLA_` (`USERNAME`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`KD_TRANSAKSI`),
  ADD KEY `FK_MELAKUKAN` (`USERNAME`),
  ADD KEY `FK_RELATIONSHIP_7` (`KD_OBAT`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `detil_transaksi`
--
ALTER TABLE `detil_transaksi`
  ADD CONSTRAINT `FK_MEMILIKI` FOREIGN KEY (`KD_TRANSAKSI`) REFERENCES `transaksi` (`KD_TRANSAKSI`),
  ADD CONSTRAINT `FK_RELATIONSHIP_5` FOREIGN KEY (`KD_OBAT`) REFERENCES `obat` (`KD_OBAT`);

--
-- Constraints for table `obat`
--
ALTER TABLE `obat`
  ADD CONSTRAINT `FK_MENGELOLA` FOREIGN KEY (`USERNAME`) REFERENCES `admin` (`USERNAME`),
  ADD CONSTRAINT `FK_RELATIONSHIP_6` FOREIGN KEY (`ID_SUPPLIER`) REFERENCES `supplier` (`ID_SUPPLIER`);

--
-- Constraints for table `supplier`
--
ALTER TABLE `supplier`
  ADD CONSTRAINT `FK_MENGELOLA_` FOREIGN KEY (`USERNAME`) REFERENCES `admin` (`USERNAME`);

--
-- Constraints for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `FK_MELAKUKAN` FOREIGN KEY (`USERNAME`) REFERENCES `admin` (`USERNAME`),
  ADD CONSTRAINT `FK_RELATIONSHIP_7` FOREIGN KEY (`KD_OBAT`) REFERENCES `obat` (`KD_OBAT`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
