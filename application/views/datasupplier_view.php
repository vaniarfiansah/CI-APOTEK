<div class="row">
  <div class="col-lg-12">
      <h1 class="page-header">Tabel Supplier</h1>
  </div>
  <div class="col-lg-3">
    <a href="<?php echo base_url();?>index.php/supplier"  <button type="button" class="btn btn-outline btn-success">Tambah Supplier</button></a>
  </div>
</br>
<div class="col-lg-12">
</br>
    <div class="panel panel-default">
        <div class="panel-heading">
            Tabel Supplier
        </div>

        <!-- /.panel-heading -->
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>ID Supplier</th>
                            <th>Nama Supplier</th>

                            <th>Alamat </th>
                            <th>Kota </th>
                            <th>Telepon</th>

                            <th> Aksi </th>
                        </tr>
                        <?php
                        $no = 1;
                        foreach ($supplier as $data) {
                          # code...
                          echo'
                            <tr>
                              <td>'.$no.'</td>
                              <td>'.$data->ID_SUPPLIER.'</td>
                              <td>'.$data->NAMA_SP.'</td>
                              <td>'.$data->ALAMAT.'</td>
                              <td>'.$data->KOTA.'</td>
                              <td>'.$data->TELP.'</td>


                              <td><a href="#" class="btn btn-info btn-sm"><i class="glyphicon glyphicon-search"></i>Lihat</a>
                              <a href="'.base_url().'index.php/supplier/hapus/'.$data->ID_SUPPLIER.'" class="btn btn-danger btn-sm"><i class="glyphicon glyphicon-trash"></i>Hapus</a>
                              </td>
                            </tr>
                          ';
                          $no++;
                        }

                         ?>
                    </thead>

                </table>
            </div>
            <!-- /.table-responsive -->
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>
