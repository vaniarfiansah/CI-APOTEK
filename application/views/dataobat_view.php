<div class="row">
  <div class="col-lg-12">
      <h1 class="page-header">Tabel Obat</h1>
  </div>
  <div class="col-lg-3">
    <a href="<?php echo base_url();?>index.php/obat"  <button type="button" class="btn btn-outline btn-success">Tambah Obat</button></a>
  </div>
</br>
<div class="col-lg-12">
</br>
    <div class="panel panel-default">
        <div class="panel-heading">
            Tabel Obat
        </div>

        <!-- /.panel-heading -->
        <div class="panel-body">

            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kode Obat</th>
                            <th>Nama Obat</th>
                            <th>Jumlah Stok</th>
                            <th>ID Supplier </th>
                            <th>Harga </th>
                            <th>Produsen</th>
                            <th> Foto </th>
                            <th> Aksi </th>
                        </tr>
                        <?php
                        $no = 1;
                        foreach ($obat as $data) {
                          # code...
                          echo'
                            <tr>
                              <td>'.$no.'</td>
                              <td>'.$data->KD_OBAT.'</td>
                              <td>'.$data->NAMA_OBAT.'</td>
                              <td>'.$data->JML_STOK.'</td>
                              <td>'.$data->ID_SUPPLIER.'</td>
                              <td>'.$data->HARGA.'</td>
                              <td>'.$data->PRODUSEN.'</td>
                              <td><img width="100pxs" src="'.base_url().'uploads/'.$data->FOTO.' " ></td>
                              <td>
                              <a href="'.base_url().'index.php/obat/hapus/'.$data->KD_OBAT.'" class="btn btn-danger btn-sm"><i class="glyphicon glyphicon-trash"></i>Hapus</a>
                              <a href="'.base_url().'index.php/obat/tambah_stok/'.$data->KD_OBAT.'"  class="btn btn-info btn-sm"><i class="glyphicon glyphicon-plus"></i>Tambah</a>
                              </td>

                            </tr>
                          ';
                          $no++;
                        }

                         ?>
                    </thead>

                </table>
            </div>
            <!-- /.table-responsive -->
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>
