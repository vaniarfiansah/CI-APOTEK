<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
          Transaksi
        </h1>
    </div>
</div>
<!-- /.row -->

<?php if (!empty($notif)) {
      echo '<div class="alert alert-success">';
      echo $notif;
      echo '</div>';
} ?>

<div class="row">
  <div class="col-lg-6 col-md-6">
    <form method="post" id="form-pendaftaran" enctype="multipart/form-data" action="<?php echo base_url();?>index.php/penjualan/save_transaksi">
      <div class="row">
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            <select name="admin" class="form-control">
                <?php
                  foreach ($admin as $a) {
                    # code...
                    echo '<option value="'.$a->USERNAME.'">'.$a->USERNAME.'</option>';
                  }
                 ?>
            </select>
              <label>KD Transaksi</label>
              <input class="form-control"  name="kd_transaksi"    >
              <label>Nama Obat</label>
              <select class="form-control" name="obat">
                <?php
                  foreach ($obt as $s) {
                    echo '<option value="'.$s->KD_OBAT.'">'.$s->NAMA_OBAT.'</option>';
                  }
                 ?>
              </select>
              <label>Jumlah</label>
              <input type="number" class="form-control" name="JUMLAH_BELI"  placeholder="Masukan" >

             <br><br>
              <input type="submit" class="btn btn-success" name="submit" value="kirim">
          </div>
        </div>
      </div>
    </form>

  </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header">
            Daftar Transaksi
        </h3>
    </div>
</div>
<!-- /.row -->

<div class="row">
  <div class="col-lg-9 col-md-9">
    <div class="table-responsive">
        <table class="table table-bordered table-hover table-striped">
            <thead>
                <tr>
                    <th>KD Transaksi</th>
                    <th>Nama Obat</th>
                    <th>Jumlah</th>
                    <th>Satuan</th>
                    <th>Tanggal Transaksi</th>
                    <th>Total Harga</th>

                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
              <?php
                  foreach ($transaksi as $data) { //ngabsen data
              echo
              '<tr>
                  <td>'.$data->KD_TRANSAKSI.'</td>
                  <td>'.$data->NAMA_OBAT.'</td>
                  <td>'.$data->JUMLAH_BELI.'</td>
                  <td>'.$data->HARGA.'</td>
                  <td>'.$data->TANGGAL.'</td>
                  <td>'.$data->JUMLAH_BELI*$data->HARGA.'</td>
                  <td>

                  <a href="'.base_url().'index.php/penjualan/hapus/'.$data->KD_TRANSAKSI.'" class="btn btn-danger btn-sm"><i class="glyphicon glyphicon-trash"></i> Hapus</a>
                  </td>
              </tr>';
              }
              ?>


            </tbody>
        </table>
    </div>
  </div>
</div>
