<div class="row">
  <div class="col-lg-12">
      <h1 class="page-header">Input Data Obat</h1>
  </div>
    <div class="col-lg-6">
      <?php
        if(!empty($notif)){
          echo'<div class="alert alert-danger">';
          echo $notif;
          echo'</div>';
        }
      ?>
        <form role="form" method="post" action="<?php echo base_url();?>index.php/obat" enctype="multipart/form-data">
          <div class="form-group">
              <label>Kode Obat</label>
              <input class="form-control" name="kd_obat" type="text" placeholder="Kode Obat">
          </div>
            <div class="form-group">
                <label>Nama obat</label>
                <input class="form-control" name="nama_obat" type="text" placeholder="Nama Obat">
            </div>
            <div class="form-group">
                <label>Jumlah Stok</label>
                <input class="form-control" name="jlm_stok" type="number" placeholder="Julah Stok">
            </div>
            <div class="form-group">
                <label>ID Supplier</label>
                <input class="form-control" name="id_supplier" type="text" placeholder="ID Supplier">
            </div>
            <div class="form-group">
                <label>Harga</label>
                <input class="form-control" name="harga" type="number" placeholder="Harga">
            </div>
            <div class="form-group">
                <label>Produsen</label>
                <input class="form-control" name="produsen" type="text" placeholder="Produsen">
            </div>
            <div class="form-group">
                <label>Foto</label>
                <input type="file" name="foto">
            </div>
            <input name="submit" type="submit" value="simpan" class="btn btn-success">
          </form>

      </div>
  </div>
