<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Penambahan Stok Obat
        </h1>
    </div>
</div>
<!-- /.row -->

<?php if (!empty($notif)) {
      echo '<div class="alert alert-success">';
      echo $notif;
      echo '</div>';
} ?>

<div class="row">
  <div class="col-lg-6 col-md-6">
    <form method="post" id="form-pendaftaran" enctype="multipart/form-data" action="<?php echo base_url();?>index.php/obat/update_stok">
      <div class="row">
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
              <label>ID Obat</label>
              <input class="form-control"  name="kd_obat" value="<?php echo $detil->KD_OBAT ?>" readonly="readonly"  >
              <label>Nama Obat</label>
              <input type="text" class="form-control" name="nama_obat" value="<?php echo $detil->NAMA_OBAT ?>" readonly="readonly">
              <label>Jumlah</label>
              <input type="number" class="form-control" name="jml_stok"  placeholder="Masukan" >

                          <br><br>
              <input type="submit" class="btn btn-success" name="submit" value="kirim">
          </div>
        </div>
      </div>
    </form>

  </div>
</div>
