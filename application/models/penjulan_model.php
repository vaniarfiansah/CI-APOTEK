<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penjualan_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }
  public function delete($id_transaksi)
  {
    $this->db->where('KD_TRANSAKSI', $id_transaksi)
             ->delete('transaksi');

             if ($this->db->affected_rows() > 0) {
               # code...
               return TRUE;
             } else {
               return FALSE;
             }
  }
}
