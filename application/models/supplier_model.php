<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }
  public function insert()
  {
    # code...
    $data = array(
      'ID_SUPPLIER' => $this->input->post('id_supplier') ,
      'NAMA_SP'=> $this->input->post('nama_sp'),
      'ALAMAT' => $this->input->post('alamat'),
      'KOTA' => $this->input->post('kota'),
      'TELP' => $this->input->post('telp'),

    );
    $this->db->insert('supplier', $data);
    if ($this->db->affected_rows() > 0) {
      # code...
      return TRUE;
    } else {
      return FALSE;
    }
  }
  public function get_data_supplier(){
    return $this->db->order_by('ID_SUPPLIER')
                    ->get('supplier')
                    ->result();
  }

  public function get_data_supplier_by_id($kd_supplier){
    return $this->db->where('ID_SUPPLIER',$kd_supplier)
                    ->get('supplier')
                    ->row();
  }

  public function delete($kd_supplier)
  {
    $this->db->where('ID_SUPPLIER',$kd_supplier)->delete('supplier');
              if ($this->db->affected_rows()>0) {
                # code...
                return TRUE;
              } else {

                return FALSE;
              }
  }
}
