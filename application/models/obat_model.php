<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Obat_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }
  public function insert($foto)
  {
    # code...
    $data = array(
      'KD_OBAT' => $this->input->post('kd_obat') ,
      'NAMA_OBAT'=> $this->input->post('nama_obat'),
      'ID_SUPPLIER' => $this->input->post('id_supplier'),
      'JML_STOK' => $this->input->post('jlm_stok'),
      'HARGA' => $this->input->post('harga'),
      'PRODUSEN' => $this->input->post('produsen'),
      'FOTO' => $foto['file_name']
    );
    $this->db->insert('obat', $data);
    if ($this->db->affected_rows() > 0) {
      # code...
      return TRUE;
    } else {
      return FALSE;
    }
  }

  public function get_data_obat(){
    return $this->db->order_by('KD_OBAT')
                    ->get('obat')
                    ->result();
  }
  public function get_data_obat_by_id($idobat){
    return $this->db->where('KD_OBAT','$idobat')
                    ->get('obat')
                    ->row();
  }
  public function delete($idobat)
  {
    $this->db->where('KD_OBAT',$idobat)->delete('obat');
    if($this->db->affected_rows()>0){
      return TRUE;
    }
    else {
      return FALSE;
    }
  }
  function kurang($id_obat, $kurangi)
  {
    $object = array('JML_STOK' => $kurangi );
    $this->db->where('KD_OBAT', $id_obat)->update('obat', $object);
  }
  public function tambah($KD_OBAT, $tambah)
  {
    $data = array('JML_STOK' => $tambah);
    $this->db->where('KD_OBAT',$KD_OBAT)->update('obat',$data);
  }

  public function getStokObat($id_obat)
  {
    $this->db->select('JML_STOK')->from('obat')->where('KD_OBAT',$id_obat);
    $query = $this->db->get();
    if ($query->num_rows() == 1) {
      # code...
      $sql =  $query->row();
      return $sql->JML_STOK;
    }
  }

  public function add_stock($id_obat)
  {
    return $this->db->where('KD_OBAT',$id_obat)
                ->get('obat')
                ->row();

  }
  public function get_data_transaksi()
  {
    $data=array();
    $this->db->select('*');
    $this->db->from('transaksi');
    $this->db->join('obat', 'transaksi.KD_OBAT = obat.KD_OBAT','inner');
    $this->db->order_by('KD_TRANSAKSI');
    $hasil=$this->db->get();

    if ($hasil->num_rows()>0) {
      # code...
      $data=$hasil->result();
    }
    $hasil->free_result();
    return $data;
  }
  public function dropdown_obat() {
    return $this->db->select('KD_OBAT, NAMA_OBAT')
                    ->get('obat')
                    ->result();
  }
  public function dropdown_admin() {
    return $this->db->select('USERNAME')->get('admin')->result();
  }

  public function save_penjualan()
  {
    $KD_TRANSAKSI = $this->input->post('kd_transaksi');
    $TANGGAL = date("Y-m-d");
    $JUMLAH_BELI = $this->input->post('JUMLAH_BELI');
    $KD_OBAT = $this->input->post('obat');
    $data = array('KD_TRANSAKSI' => $KD_TRANSAKSI,
                  'TANGGAL' => $TANGGAL,
                  'JUMLAH_BELI' => $JUMLAH_BELI,
                  'KD_OBAT' => $KD_OBAT
   );

   $this->db->insert('transaksi',$data);
   if ($this->db->affected_rows() == 1) {
     # code...
     return TRUE;
   } else {
     return FALSE;
   }
  }


  function getJumlahJual($id_jual){
    $this->db->select('JUMLAH_BELI')->from('transaksi')->where('KD_TRANSAKSI',$id_jual);
    $query = $this->db->get();
    if ($query->num_rows() == 1) {
      # code...
      $sql = $query->row();
      return $sql->JUMLAH_BELI;
    }
  }


}
