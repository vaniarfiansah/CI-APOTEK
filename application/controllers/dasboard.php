<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dasboard extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    		$this->load->library('form_validation');
  }

  function index()
  {
    $data['main_view'] = 'dasboard_view';
    $this->load->view('template',$data);
  }

}
