  <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penjualan extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('obat_model');
        $this->load->helper('url');
  }

  function index()
  {
    $data['main_view'] = 'penjualan_view';
        $data['obt'] = $this->obat_model->dropdown_obat();
    $data['transaksi'] = $this->obat_model->get_data_transaksi();
    $data['admin'] = $this->obat_model->dropdown_admin();

    $hasil = $this->session->flashdata('hasil');
    if ($hasil == 'berhasil') {
      # code...
      $data['notif'] = 'Transaksi sukses';
    } else if($hasil == 'kurang') {
      $data['notif'] = 'Stok Kurang';
    } else if($hasil == 'gagal'){
      $data['notif']== 'Transaksi Gagal';
    }
        $this->load->view('template',$data);

  }

  public function save_transaksi()
  {
    if ($this->input->post('submit')) {
      # code...
      $this->form_validation->set_rules('kd_transaksi', 'KD transaksi', 'trim|required');
      $this->form_validation->set_rules('JUMLAH_BELI', 'Jumlah', 'trim|required');


      if ($this->form_validation->run() == TRUE) {
        # code...
        $obt = $this->input->post('obat');
        $jml = $this->input->post('JUMLAH_BELI');
        $getobt = $this->obat_model->getStokObat($obt);

        if ($jml > $getobt) {
          # code...
          $this->session->set_flashdata('hasil','kurang');
          redirect(base_url().'index.php/penjualan');
        }

        if ($this->obat_model->save_penjualan() == TRUE) {
          # code...
          $obat = $this->input->post('obat');
          $kd_transaksi = $this->input->post('kd_transaksi');
          $minta = $this->input->post('jml_stok');
          $stok = $this->obat_model->getStokObat($obat);
          $jml = $this->obat_model->getJumlahJual($kd_transaksi);
          $kurangi = (int)$stok - $jml;
          $this->obat_model->kurang($obat,$kurangi);

          $this->session->set_flashdata('hasil','berhasil');
          redirect(base_url().'index.php/penjualan');


        } else {
          $this->session->set_flashdata('hasil','gagal');
            redirect(base_url().'index.php/penjualan');
        }
      }
      else {
          $data['notif'] = validation_errors();
          $data['main_view'] = 'penjualan_view';
          $this->load->view('template',$data);

        }
    }
    }

    public function hapus() {
      $idobat = $this->uri->segment(3);

      if ($this->obat_model->delete($id_transaksi)== TRUE) {
        # code...
        $this->session->set_flashdata('notif', 'Hapus Data Berhasil');
        redirect('penjualan');
      } else {
        $this->session->set_flashdata('notif', 'Hapus data gagal');
        redirect('penjualan');
      }
    }
}
