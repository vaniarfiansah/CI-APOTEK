<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
      	$this->load->library('form_validation');
        $this->load->model('supplier_model');
  }

  function index()
  {
    if ($this->input->post('submit')) {
      # code...
      $this->form_validation->set_rules('id_supplier','ID Supplier','trim|required');
      $this->form_validation->set_rules('nama_sp','Nama Supplier','trim|required');
      $this->form_validation->set_rules('alamat','Alamat','trim|required');
        $this->form_validation->set_rules('kota','Kota','trim|required');
      $this->form_validation->set_rules('telp','Telpon','trim|required');

      if ($this->form_validation->run() ==  TRUE) {
        if ($this->supplier_model->insert() == TRUE) {
          # code...
          $data['main_view'] = 'supplier_view';
          $data['notif'] = 'Data berhasil disimpan';
          $this->load->view('template',$data);
        } else {
          $data['main_view'] = 'supplier_view';
          $data['notif'] = 'Data gagal disimpan';
          $this->load->view('template',$data);
        }
      } else {
        $data['main_view'] = 'supplier_view';
        $data['notif'] = validation_errors();
        $this->load->view('template',$data);
      }




    } else {
      $data['main_view'] = 'supplier_view';
      $this->load->view('template',$data);
    }


  }

  public function datasupplier()
  {
    $data['supplier'] = $this->supplier_model->get_data_supplier();
    $data['main_view'] = 'datasupplier_view';
    $this->load->view('template',$data);
  }

  public function hapus() {
    $kd_supplier = $this->uri->segment(3);
    if ($this->supplier_model->delete($kd_supplier)==TRUE) {
      # code...
      $this->session->set_flashdata('notif','Hapus Data Berhasil');
      redirect('supplier/datasupplier');
    } else {

      $this->session->set_flashdata('notif','Hapus Data gagal');
      redirect('supplier/datasupplier');
    }
  }


}
