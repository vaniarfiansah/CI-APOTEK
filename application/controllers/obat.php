<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Obat extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    	$this->load->library('form_validation');
          $this->load->model('obat_model');
              $this->load->helper('url');
  }

  public function index()
  {

    if ($this->input->post('submit')) {
      # code...
      $this->form_validation->set_rules('kd_obat','Kode Obat','trim|required');
      $this->form_validation->set_rules('nama_obat','Nama Obat','trim|required');
      $this->form_validation->set_rules('jlm_stok','Jumlah Stok','trim|required');
        $this->form_validation->set_rules('id_supplier','ID Supplier','trim|required');
      $this->form_validation->set_rules('harga','Harga','trim|required');
      $this->form_validation->set_rules('produsen','Produsen','trim|required');


      if ($this->form_validation->run() ==  TRUE) {

        $config['upload_path']='./uploads/';
        $config['allowed_types'] = 'jpg|gif|png';
        $config['max_size'] = 2000;

        $this->load->library('upload', $config);
        if ($this->upload->do_upload('foto')) {
          if ($this->obat_model->insert($this->upload->data()) == TRUE) {
                # code...
                $data['main_view'] = 'inputobat_view';
                $data['notif'] = 'Data Berhasil Di Masukkan';
                $this->load->view('template',$data);
              } else {
                $data['main_view'] = 'inputobat_view';
                $data['notif']  = 'Data gagal dimasukkan';
                $this->load->view('template',$data);
              }
            }
          else {
              $data['main_view'] = 'inputobat_view';
              $data['notif'] = $this->upload->display_errors();
              $this->load->view('template', $data);
              }
    } else {
      $data['main_view'] = 'inputobat_view';
      $data['notif'] = validation_errors();
      $this->load->view('template',$data);
    }
  }
else {
  $data['main_view'] = 'inputobat_view';
  $this->load->view('template',$data);
}
}


  public function dataobat()
  {
    $data['obat'] = $this->obat_model->get_data_obat();
    $data['main_view'] = 'dataobat_view';
    $this->load->view('template',$data);

  }

  public function hapus() {
    $idobat = $this->uri->segment(3);

    if ($this->obat_model->delete($idobat)== TRUE) {
      # code...
      $this->session->set_flashdata('notif', 'Hapus Data Berhasil');
      redirect('obat/dataobat');
    } else {
      $this->session->set_flashdata('notif', 'Hapus data gagal');
      redirect('obat/dataobat');
    }
  }

  public function tambah_stok()
  {

    $data['detil'] = $this->obat_model->add_stock($this->uri->segment(3));
    $data['main_view'] = 'tambahstok_view';
    $this->load->view('template',$data);

  }
  public function update_stok()
  {
    if ($this->input->post('submit')) {
      # code...
      $id_obat = $this->input->post('kd_obat');
      $jmlstok = $this->input->post('jml_stok');
      $stok = $this->obat_model->getStokObat($id_obat);
      $tambah = (int)$jmlstok + $stok;
      $this->obat_model->tambah($id_obat,$tambah);
      redirect(base_url().'index.php/obat/dataobat');
    }
  }

  public function lihat()
  {
      if ($this->session->userdata('logged_in') == TRUE)
       {
        # code...
        $data['main_view'] = 'detil_obat_view';
        $id_obat = $this->uri->segment(3);

        $data['detil'] = $this->obat_model->get_data_obat_by_id($id_obat);
        $this->load->view('template',$data);
      } else {

        redirect('admin');
      }
  }

}
