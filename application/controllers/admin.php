<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model('admin_model');
  }

  function index()
  {
    if ($this->session->userdata('logged_in') == TRUE) {
      # code...
      redirect(base_url('index.php/dasboard'));
    } else {
        $this->load->view('login_view');
    }

  }
public function do_login()
{
  # code...
  if ($this->input->post('submit')) {
    # code...
    $this->form_validation->set_rules('username','Username', 'trim|required');
    $this->form_validation->set_rules('password','Password', 'trim|required');

    if ($this->form_validation->run() == TRUE) {
      # code...
      if ($this->admin_model->cek_user() == TRUE) {
        # code...
        redirect(base_url('index.php/dasboard'));
      }
      else {

        $data['notif'] = 'Login Gagal';
        $this->load->view('login_view',$data);

      }
    } else {

      $data['notif'] = validation_errors();
      $this->load->view('login_view',$data);
    }
  }
}

public function logout()
{

  $data = array(
    'username'	=> '',
    'logged_in'	=> FALSE
  );
  $this->session->sess_destroy();
  redirect(base_url('index.php/admin'));


}


}
